<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Saint Paul University Philippines</title>
	<link rel="shortcut icon" href="spuplogo.png"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-1">
				<img src="spuplogo.png" alt="" class="img-responsive text-center" height="50px" width="50px">
			</div>
			<div class="col-md-3">
				<h4 style="font-family: Old English Text MT;"  class="">St. Paul University Philippines</h5>
				<h6>Tuguegarao City, Cagayan 3500</h4>
			</div>
		</div> <hr style="height: 10px; border: 0; box-shadow: 0 10px 10px -10px #63bf35 inset;">
		<div class="row">
			<ul class="nav nav-pills">
				<li class="active"><a href="#">Home</a></li>
				<li><a href="history.html">History</a></li>
				<li><a href="vi-mi.html">Vision-Mission</a></li>
				<li><a href="core-val.html">Core Values</a></li>
				<li><a href="award.html">Awards</a></li>
			</ul>
		</div> <hr style="height: 10px; border: 0; box-shadow: 0 10px 10px -10px #63bf35 inset;">
		<div class="row">
			<div class="col-md-3">
				<h3 class="text-center" style="font-family: Old English Text MT">Achievers</h3><hr style="border-top: 3px double #63bf35;">
				<ul class="list-unstyled text-center">
					<li><strong>John Kit Masigan</strong></li>
					<li>TOSP 2014</li><br>
					<li><strong>Giovanni Cabalza</strong></li>
					<li>TOSP Region 02 2015 Awardee</li> <br>
					<li><strong>Karylle Mae Formoso</strong></li>
					<li>TOSP Region 02 2015 Awardee</li> <br>
					<li><strong>Nelson Gacutan</strong></li>
					<li>Outstanding UNESCO Youth Leaders of the Philippines 2013 Awardee</li> <br>
					<li><strong>Mark Anthony Baliuag</strong></li>
					<li>Delegate, 29th International Youth Leadership Conference in Prague, Czech Republic</li> <br>
					<li><strong>Jayson Tacazon</strong></li>
					<li>Top 7, 2014 Sanitary Engineering Board Examination</li> <br>
				</ul>
			</div>
			<div class="col-md-6">
				<h3 class="text-center" style="font-family: Old English Text MT">News</h3> <hr style="border-top: 3px double #63bf35;">
				<div class="row">
					<div class="col-md-4">
						<img src="1.jpg" alt="" class="img-responsive">
					</div>
					<div class="col-md-8">
						<p><strong>SPUP INVITES PNP-TUGUEGARAO FOR SHS SAFETY AND SECURITY ORIENTATION </strong></p>
						<p> Caritas Christi urget nos! As part of the week-long Orientation Program for the Senior High School (SHS), SPUP has invited officers from the... <a href="news1.html">See more</a></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<img src="2.jpg" alt="" class="img-responsive">
					</div>
					<div class="col-md-8">
						<p><strong>SPUP’S PVCD REAPS AWARDS IN UNESCO CLUBS INTERNATIONAL ASSEMBLY</strong></p>
						<p> Caritas Christi urget nos! (by: Ms. Noemi Cabaddu and Dr. Allan Peejay Lappay) The Paulinian Volunteers for Community... <a href="news2.html">See more</a></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<img src="3.jpg" alt="" class="img-responsive">
					</div>
					<div class="col-md-8">
						<p><strong>SPUP'S HEALTH SERVICES REACHES OUT TO RHWC </strong></p>
						<p> Caritas Christi urget nos! (by: Ms. Zylla Ezra Tenedero and Mr. Jaypee Talosig) SPUP's Health Services Unit (HSU) initiated an Outreach Activity at the Regional Haven for... <a href="news3.html">See more</a></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<img src="4.jpg" alt="" class="img-responsive">
					</div>
					<div class="col-md-8">
						<p><strong>SPUP-MEDTECH YIELDS 90.91% PASSING RATE </strong></p>
						<p> Caritas Christi urget nos! SPUP's College of Medical Technology (MedTech) registered a passing rate of 90.91%. This was announced by the Professional Regulation Commission (PRC)... <a href="news4.html">See more</a></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<img src="12.jpg" alt="" class="img-responsive">
					</div>
					<div class="col-md-8">
						<p><strong>SPUP RECOGNIZES INTERNS FROM CJCU, TAIWAN </strong></p>
						<p> Caritas Christi urget nos! St. Paul University Philippines (SPUP) through the Intercultural Institute for Languages (IIL) and the Graduate School, awarded Certificate of Completion and Certificate of Recognition to... <a href="news5.html">See more</a></p>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<h3 class="text-center" style="font-family: Old English Text MT">Events</h3><hr style="border-top: 3px double #3552bf;">
			</div>
		</div>
	</div> <!--end container-->
</body>
</html>